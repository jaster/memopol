function parltrack_download_pipe() {
    [ -n "$CLEAN" ] && rm -rf $1
    [ -f "$1" ] || wget http://parltrack.euwiki.org/dumps/$1 || exit 1

    export DJANGO_SETTINGS_MODULE=memopol.settings
    unxz -c ${OPENSHIFT_DATA_DIR}$1 | $2
    [ -n "$CLEAN" ] && rm -rf $1
}

function francedata_download_pipe() {
    [ -n "$CLEAN" ] && rm -rf $1
    [ -f "$1" ] || wget https://francedata-njoyard.rhcloud.com/$1 || exit 1

    export DJANGO_SETTINGS_MODULE=memopol.settings
    gunzip -c ${OPENSHIFT_DATA_DIR}$1 | $2
    [ -n "$CLEAN" ] && rm -rf $1
}

function nosdeputes_download_pipe() {
    [ -n "$CLEAN" ] && rm -rf $1
    [ -f "$1" ] || bin/utils/nosdeputes_importer $3 $1 || exit 1

    export DJANGO_SETTINGS_MODULE=memopol.settings
    gunzip -c ${OPENSHIFT__DIR}$1 | $2
    [ -n "$CLEAN" ] && rm -rf $1
}

function refresh_scores() {
    export DJANGO_SETTINGS_MODULE=memopol.settings
    memopol refresh_scores
}
